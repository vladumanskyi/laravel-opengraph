<?php
/**
 * ==============================
 * Open Graph configuration file
 * =============================
 */

return  [
    'config_classes' => [
        'getBasic'       => \VU\OpenGraph\Properties\Basic::class,
        'getImage'       => \VU\OpenGraph\Properties\Image::class,
        'getMusic'       => \VU\OpenGraph\Properties\Music::class,
        'getVideo'       => \VU\OpenGraph\Properties\Video::class,
        'getAudio'       => \VU\OpenGraph\Properties\Audio::class,
        'getArticle'     => \VU\OpenGraph\Properties\Article::class,
        'getBook'        => \VU\OpenGraph\Properties\Book::class,
        'getProfile'     => \VU\OpenGraph\Properties\Profile::class,
        'useTwitterCard' => \VU\OpenGraph\Properties\TwitterCard::class,
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'       => '',
            'description' => '',
            'url'         => '',
            'type'        => '',
            'site_name'   => '',
            'images'      => [],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
            //'card'        => '',
            //'site'        => '',
        ],
    ],
];
