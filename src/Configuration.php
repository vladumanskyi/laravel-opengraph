<?php
declare(strict_types=1);

namespace VU\LaravelOpenGraph;

use VU\LaravelOpenGraph\Contracts\OpenGraphConfiguration;
use VU\LaravelOpenGraph\Exceptions\ConfigException;

class Configuration implements OpenGraphConfiguration
{
    /**
     * @var array
     */
    private $config;

    /**
     * Configuration constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @return array
     */
    public function config(): array
    {
        return $this->config;
    }

    /**
     * @return array
     * @throws ConfigException
     */
    public function tags()
    {
        if (empty($this->config['config_classes'])) {
            throw new ConfigException('Missing config_class data');
        }

        return $this->config['config_classes'];
    }
}
