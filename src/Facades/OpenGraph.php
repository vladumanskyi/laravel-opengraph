<?php

declare(strict_types=1);

namespace VU\LaravelOpenGraph\Facades;

use Illuminate\Support\Facades\Facade;

class OpenGraph extends Facade
{
    /**
     * {@inheritdoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'vu.opengraph';
    }
}
