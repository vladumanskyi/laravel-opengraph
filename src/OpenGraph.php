<?php

declare(strict_types=1);

namespace VU\LaravelOpenGraph;

use Illuminate\Support\Facades\App;
use VU\LaravelOpenGraph\Contracts\OpenGraph as BaseOpenGraph;
use VU\LaravelOpenGraph\Contracts\OpenGraphConfiguration as Configuration;
use VU\OpenGraph\Exceptions\OpenGraphException;
use VU\OpenGraph\Configuration as Property;

/**
 * @method \VU\OpenGraph\Properties\Article     getArticle()
 * @method \VU\OpenGraph\Properties\Image       getImage()
 * @method \VU\OpenGraph\Properties\Audio       getAudio()
 * @method \VU\OpenGraph\Properties\Book        getBook()
 * @method \VU\OpenGraph\Properties\Basic       getBasic()
 * @method \VU\OpenGraph\Properties\Music       getMusic()
 * @method \VU\OpenGraph\Properties\Profile     getProfile()
 * @method \VU\OpenGraph\Properties\Video       getVideo()
 * @method \VU\OpenGraph\Properties\TwitterCard useTwitterCard()
 */
final class OpenGraph implements BaseOpenGraph
{
    private Configuration $configuration;

    /**
     * OpenGraph constructor.
     * @param Configuration $configuration
     */
    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @param string $name
     * @param array  $arguments
     *
     * @throws OpenGraphException
     *
     * @return object
     */
    public function __call(string $name, array $arguments = [])
    {
        $tags = $this->configuration()->tags();

        if (!array_key_exists($name, $tags)) {
            throw new OpenGraphException(sprintf('The method %s does not exist', $name));
        }

        return new $tags[$name]($this->property());
    }

    /**
     * @return Configuration
     */
    public function configuration(): Configuration
    {
        return $this->configuration;
    }

    /**
     * @return Property
     */
    public function property(): Property
    {
        return App::make(Property::class);
    }
}
