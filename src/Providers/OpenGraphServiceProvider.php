<?php

declare(strict_types=1);

namespace VU\LaravelOpenGraph\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use VU\LaravelOpenGraph\Contracts\OpenGraphConfiguration as ContractOGConfiguration;
use VU\LaravelOpenGraph\Configuration;
use VU\LaravelOpenGraph\Contracts\OpenGraph as ContractOpenGraph;
use VU\LaravelOpenGraph\OpenGraph;
use VU\OpenGraph\PropertyConfiguration;
use VU\OpenGraph\Render;
use VU\OpenGraph\RenderHandler;

class OpenGraphServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function boot()
    {
        $configFile = __DIR__ . '/../config/open-graph.php';

        if ($this->isLumen()) {
            $this->app->configure('open-graph');
        } else {
            $this->publishes([
                $configFile => config_path('open-graph.php'),
            ]);
        }

        $this->mergeConfigFrom($configFile, 'open-graph');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('vu.opengraph.config', function ($app) {
            return new Configuration($app['config']->get('open-graph', []));
        });

        $this->app->singleton('vu.opengraph', function ($app) {
            return new OpenGraph($app->make('vu.opengraph.config'));
        });

        $this->app->bind(ContractOGConfiguration::class, 'vu.opengraph.config');
        $this->app->bind(ContractOpenGraph::class, 'vu.opengraph');

        $this->app->bind(RenderHandler::class, Render::class);

        $this->app->bind(\VU\OpenGraph\Configuration::class, function ($app) {
            return new PropertyConfiguration($app->make(RenderHandler::class));
        });
    }

    /**
     * {@inheritdoc}
     */
    public function provides()
    {
        return [
            ContractOGConfiguration::class,
            ContractOpenGraph::class,
            'vu.opengraph',
            'vu.opengraph.config',
        ];
    }

    /**
     * @return bool
     */
    protected function isLumen(): bool
    {
        return Str::contains($this->app->version(), 'Lumen');
    }
}
