<?php

declare(strict_types=1);

namespace VU\LaravelOpenGraph\Exceptions;

use Throwable;

class ConfigException extends \Exception
{
    /**
     * @var string
     */
    const MESSAGE = '[x] Something went wrong with Open Graph configuration';

    /**
     * ConfigException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = self::MESSAGE, $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
