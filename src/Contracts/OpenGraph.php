<?php

declare(strict_types=1);

namespace VU\LaravelOpenGraph\Contracts;

interface OpenGraph
{
    /**
     * @return OpenGraphConfiguration
     */
    public function configuration(): OpenGraphConfiguration;
}
