<?php

declare(strict_types=1);

namespace VU\LaravelOpenGraph\Contracts;


interface OpenGraphConfiguration
{
    /**
     * @return array
     */
    public function config(): array;
}
