# Changelog

All notable changes to `php-open-graph-library` will be documented in this file

## 2.0.0 - 2024-08-11

- Stop supporting Laravel 8.0
- Support php 8.1
- Support Laravel 10 and 11 versions

## 1.0.0 - 2021-02-04

- Upgrade php version from 7.4 to 8.0

## 1.0.0 - 2020-12-24

- Initial release
- Added Twitter Card
